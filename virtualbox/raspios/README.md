# Команды в Raspios, выполненные через GUI, чтобы можно было работать с виртуальной машиной по ssh

Установка ssh сервера
```
sudo apt update -y
sudo apt upgrade -y

sudo apt install -y openssh-server
sudo systemctl enable ssh
sudo systemctl start ssh
```
